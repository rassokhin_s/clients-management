middleware  = require("./middleware")
CRUDBuilder = require("./crud")
_           = require("underscore")


class API

  CRUD:
    users       : new CRUDBuilder "users"
    masters     : new CRUDBuilder "masters"
    clients     : new CRUDBuilder "clients"
    visits      : new CRUDBuilder "visits"

  bindAll: (app) ->
    _.each @CRUD, (item) ->
      item.bindAll app

module.exports = API
