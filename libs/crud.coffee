_       = require("underscore")
Schemas = require '../models'


sendJSON = (err, data, res) ->
  console.log "error:", err  if err
  #            return res.send(500);
  return res.send(404)  unless data
  res.writeHead 200,
    "Content-Type": "application/json"
  res.end JSON.stringify(data)

class CRUDBuilder

  constructor: (collectionName, permissions) ->

    self = this
    @Schema = Schemas[collectionName]
    @id_key = "id"
    @collectionName = collectionName
    methods = ["insert", "findOne", "findAndModify", "find", "findAndDelete"]
    
    # permissions
    permissions or (permissions = {})
    @permissionMiddlewares = {}
    _.each methods, (n) ->
      self.permissionMiddlewares[n] = permissions[n] or (req, res, next) ->
        next()

      self[n] = (req, res) ->
        method = _.bind(self["_" + n], self, req, res)
        self.permissionMiddlewares[n].call self, req, res, method

  bindAll: (app) ->
    app.post "/server/api/" + @collectionName, _.bind(@insert, this)
    app.get "/server/api/" + @collectionName + "/:id", _.bind(@findOne, this)
    app.put "/server/api/" + @collectionName + "/:id", _.bind(@findAndModify, this)
    app.delete "/server/api/" + @collectionName + "/:id", _.bind(@findAndDelete, this)
    app.get "/server/api/" + @collectionName, _.bind(@find, this)

  _insert: (req, res) ->
    self = this
    console.log "CRUD: insert/" + @collectionName
    entity = new self.Schema(req.body)
    entity.save (err, data) ->
      if err
        console.log err
      else
        res.send entity

  _findOne: (req, res) ->
    self = this
    id = req.params[self.id_key]
    console.log "CRUD: findOne/" + self.collectionName + "/" + id
    self.Schema.findById id,
      __v: 0
    , (err, document) ->
      console.log "Found by id ", id
      sendJSON err, document, res

  _findAndModify: (req, res) ->
    self = this
    id = req.params[self.id_key]
    console.log "CRUD: findAndModify/" + self.collectionName + "/" + id
    self.Schema.findById id, (err, document) ->
      json = _.extend({}, req.body)
      delete json._id

      delete json.__v

      _.extend document, json
      document.save()
      console.log "aaaaaa", json, document
      sendJSON err, document, res

  _findAndDelete: (req, res) ->
    self = this
    id = req.params[self.id_key]
    console.log "CRUD: findAndDelete/" + self.collectionName + "/" + id
    self.Schema.findById id, (err, document) ->
      document.remove()
      sendJSON err, document, res

  _find: (req, res) ->
    self = this
    console.log "CRUD: find/" + self.collectionName
    self.Schema.find {},
      __v: 0
    , (err, documents) ->
      sendJSON err, documents, res

#        search : function(req, res){
#            console.log('search');
#            var pair = {};
#            pair[req.params['keyword']] = new RegExp(req.params['value'],'i');
#            console.log(pair);
#            Schemas[this.collectionName].find( pair, function(err, response){
#                res.send(response);
#            });
#        }    
module.exports = CRUDBuilder
