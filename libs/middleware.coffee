module.exports = requireAuth: (req, res, next) ->
  if req.session.user
    next()
  else
    res.send 401
#            next(new Error('Failed to load user ' + req.params.id))
