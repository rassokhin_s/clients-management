module.exports = 
  users       : require('./user')
  masters     : require('./master')
  clients     : require('./client')
  visits      : require('./visit')
