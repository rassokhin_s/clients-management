mongoose = require('mongoose')
Schema = mongoose.Schema
ObjectId = Schema.ObjectId;

User = new Schema
  firstName        : String
  lastName         : String
  displayName      : String
  email            : String
  password         : String

module.exports = mongoose.model 'User', User
