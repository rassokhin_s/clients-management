mongoose = require('mongoose')
Schema = mongoose.Schema
ObjectId = Schema.ObjectId;

Client = new Schema
  firstName        : String
  secondName       : String
  lastName         : String
  email            : String
  phone            : String
  comment          : String

module.exports = mongoose.model 'Client', Client
