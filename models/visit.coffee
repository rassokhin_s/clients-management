mongoose = require('mongoose')
Schema = mongoose.Schema
ObjectId = Schema.ObjectId;

Visit = new Schema
  type      : ObjectId
  userId    : ObjectId
  masterId  : ObjectId
  price     : Number
  comment   : String
  date      : { type: Date, default: Date.now }

module.exports = mongoose.model 'Visit', Visit
