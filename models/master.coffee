mongoose = require('mongoose')
Schema = mongoose.Schema
ObjectId = Schema.ObjectId;

Master = new Schema
  firstName        : String
  secondName       : String
  lastName         : String
  email            : String
  phone            : String
  type             : String
  comment          : String
  startDate        : Date

module.exports = mongoose.model 'Master', Master
