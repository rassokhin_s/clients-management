exports.index = (req, res) ->
  renderIndex
    req      : req
    res      : res
    login    : false
    register : false

exports.login = (req, res) ->
  renderIndex
    req      : req
    res      : res
    login    : true
    register : false
  
exports.register = (req, res) ->
  renderIndex
    req      : req
    res      : res
    login    : false
    register : true

renderIndex = (opts) ->
    user = opts.req.session.passport.user
    console.log '\n\n\n USER IN INDEX', opts.req.session.passport.user, '\n\n\n'

    opts.res.render 'index', 
      title: 'Менеджмент'
      provider: if user?.provider? then user.provider else null
      displayName: if user?.displayName? then user.displayName else null
      login   : opts.login
      register: opts.register
