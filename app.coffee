fs               = require('fs')
_                = require('underscore')
express          = require('express')
mongoose         = require('mongoose')
models           = require('./models')
stylus           = require('stylus')
passport         = require('passport')
passwordHash     = require('password-hash')
LocalStrategy    = require('passport-local').Strategy
UserModel        = require('./models/user')
API              = require('./libs/api')
nodemailer       = require("nodemailer")
redis            = require("redis")
RedisStore       = require("connect-redis")(express)

app = module.exports = express.createServer()

passport.serializeUser (user, done) ->
  done null, user

passport.deserializeUser (user, done) ->
  if user.provider is 'local'
    UserModel.findById user._id, (err, user) ->
      done err, user
  else
    done null, user

passport.use(new LocalStrategy({usernameField: 'email'}, (email, password, done) ->
  UserModel.findOne
    email: email
  , (err, user) ->
    return done(err)  if err
    unless user
      return done(null, false,
        message: "Incorrect username."
      )
    unless passwordHash.verify(password, user.password)
      return done(null, false,
        message: "Incorrect password."
      )
    done null, user
))

app.dynamicHelpers({
  passport: (req, res) ->
    return req.session.passport
})

#connect to mongodb
mongodb_url = process.env.MONGOHQ_URL || 'mongodb://localhost/client-management'
mongoose.connect mongodb_url

switch process.env.WHERE_TO_RUN
  when 'heroku'
    rtg = require("url").parse(process.env.REDISTOGO_URL)
    sess_redis = redis.createClient(rtg.port, rtg.hostname)
    sess_redis.auth(rtg.auth.split(":")[1])
  else
    sess_redis = redis.createClient(6379, '127.0.0.1')

sess_redis.select 0

# Configuration
app.configure ->
  app.set 'views', __dirname + '/views'
  app.set 'view engine', 'jade'
  app.set 'view options', { pretty: true }
  
  app.use express.logger()
  app.use express.cookieParser()
  app.use express.bodyParser()
  app.use express.methodOverride()
  app.use stylus.middleware(
    src: __dirname + '/public'
    dest: __dirname + '/public'
    compile: (str, path) ->
      stylus(str).set("filename", path).set("warn", true).set "compress", true
  )
  app.use express.session(
    key: "rassokhin.sid"
    secret: "2@##rfasdasdasdf0sdv@#98&#(*XZ(6x12ysa))"
    store: new RedisStore(
      client: sess_redis
      ttl: 7 * 24 * 60 * 60 # one-week in DB
    )
    cookie: # but a year cookie
      maxAge: 365 * 24 * 60 * 60 * 1000
  )
  app.use passport.initialize()
  app.use passport.session()
  app.use app.router
  app.use express.static(__dirname + '/public')

app.configure 'development', ->
  app.use express.errorHandler { dumpExceptions: true, showStack: true }

app.configure 'production', ->
  app.use express.errorHandler()

# Routes

api = new API
api.bindAll app

routes = require('./routes')

app.get '/', routes.index
app.get '/login', routes.login
app.get '/register', routes.register

app.get "/login", (req, res) ->
  res.redirect "/"

app.post "/login", passport.authenticate("local",
  failureRedirect: "/error"
  failureFlash: true
), (req, res) ->
  res.redirect "/"
  
app.post "/registration", (req, res, next) ->
  userBody = req.body
  UserModel.findOne
    email: userBody.email
  , (err, user) ->
    if user?
      res.redirect "/"
    else
      model = new UserModel
        firstName    : userBody.firstName
        lastName     : userBody.lastName
        displayName  : "#{userBody.firstName} #{userBody.lastName}"
        email        : userBody.email
        password     : passwordHash.generate userBody.password
      model.save ()->
        passport.authenticate('local', (err, user, info) ->
          console.log('arguments', arguments)
          return 0 if err
          unless user
            req.flash "error", info.message
            return res.redirect("/error")
          req.logIn user, (err) ->
            return 0 if err
            res.redirect "/"
        ) req, res, next

app.get "/logout", (req, res) ->
  req.logout()
  console.log '\n\n\n LOGOUT OK \n\n\n'
  res.redirect "/"

app.get "*", (req, res, next) ->
  return next()  if /\./.test(req.url)
  routes.index req, res, next

app.post "/sendmail", (req, res) ->
  sendMail(req.body.destination, req.body.subject, req.body.content, (msg)->
    res.json
      msg: msg
      user: req.body.displayName
      email: req.body.destination
  )

ensureAuthenticated = (req, res, next) ->
  return next()  if req.isAuthenticated()
  res.redirect "/login"

smtpTransport = nodemailer.createTransport("SMTP",
  service: "Gmail"
  auth:
    user: "sr.rassokhin@gmail.com"
    pass: "eGchRvGvRr"
)

sendMail = (destination, subject, content, cb) ->
  # setup e-mail data with unicode symbols
  mailOptions =
    from: "Logsta App <logstra@gmail.com>" # sender address
    to: destination # list of receivers
    subject: subject # Subject line
    text: content # plaintext body

  # send mail with defined transport object
  smtpTransport.sendMail mailOptions, (error, response) ->
    if error
      cb error
    else
      cb response.message

port = process.env.PORT || 3131
app.listen port, ->
  console.log "Express server listening on port %d in %s mode", app.address().port, app.settings.env
#  console.log "Open this app at http://local.host:3000 if it runs locally. Configure /etc/hosts to link local.host to 127.0.0.1"

