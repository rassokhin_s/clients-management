depends = [
  'underscore',
  'backbone',
  'cs!models/user'
]
define depends, (_, Backbone, UserModel) ->

  window.UserCollection = class UserCollection extends Backbone.Collection

    # url to read todos from
    url: '/server/api/users'

    # Reference to this collection's model.
    model: UserModel
