depends = [
  'underscore',
  'backbone',
  'cs!models/client'
]
define depends, (_, Backbone, ClientModel) ->

  window.ClientCollection = class ClientCollection extends Backbone.Collection

    # url to read todos from
    url: '/server/api/clients'

    # Reference to this collection's model.
    model: ClientModel
