depends = [
  'underscore',
  'backbone',
  'cs!models/master'
]
define depends, (_, Backbone, MasterModel) ->

  window.MasterCollection = class MasterCollection extends Backbone.Collection

    # url to read todos from
    url: '/server/api/masters'

    # Reference to this collection's model.
    model: MasterModel
