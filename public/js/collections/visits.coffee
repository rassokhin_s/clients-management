depends = [
  'underscore',
  'backbone',
  'cs!models/visit'
]
define depends, (_, Backbone, VisitModel) ->

  window.VisitCollection = class VisitCollection extends Backbone.Collection

    # url to read todos from
    url: '/server/api/visits'

    # Reference to this collection's model.
    model: VisitModel
