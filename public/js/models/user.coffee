define ['underscore', 'backbone'], (_, Backbone) ->

  window.UserModel = class UserModel extends Backbone.Model

    url: ->
      id = this.get('_id')
      '/server/api/users' + (if id then "/#{id}" else '')

    #use _id instead of id to work with mongodb
    idAttribute: "_id"
