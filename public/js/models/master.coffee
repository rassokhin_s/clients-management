define ['underscore', 'backbone'], (_, Backbone) ->

  window.MasterModel = class MasterModel extends Backbone.Model

    url: ->
      id = this.get('_id')
      '/server/api/masters' + (if id then "/#{id}" else '')

    #use _id instead of id to work with mongodb
    idAttribute: "_id"
