define ['underscore', 'backbone'], (_, Backbone) ->

  window.ClientModel = class ClientModel extends Backbone.Model

    url: ->
      id = this.get('_id')
      '/server/api/clients' + (if id then "/#{id}" else '')

    #use _id instead of id to work with mongodb
    idAttribute: "_id"
