define ['underscore', 'backbone'], (_, Backbone) ->

  window.VisitModel = class VisitModel extends Backbone.Model

    url: ->
      id = this.get('_id')
      '/server/api/visits' + (if id then "/#{id}" else '')

    #use _id instead of id to work with mongodb
    idAttribute: "_id"
