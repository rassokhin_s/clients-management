depends = [
  'jquery', 
  'underscore', 
  'backbone',
  'text!templates/main_page.html',
]

define depends, ($, _, Backbone, mainPageTemplate) ->

  class MainPageView extends Backbone.View

    el: "#content"

    template      : _.template mainPageTemplate
    
    initialize: ->
      @render()

    render: ->
      $(@el).html @template
        user: app.user
      return @
