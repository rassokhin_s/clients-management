depends = [
  'jquery'
  'underscore'
  'backbone'
  'cs!collections/users'
  'cs!collections/masters'
  'cs!collections/clients'
  'cs!collections/visits'
  'cs!routers/router'
  'jade'
]

define depends, ($, _, Backbone, UsersCollection, MastersCollection, ClientsCollection, VisitsCollection, Router) ->

  class App

    root: '/'

    constructor: ->

      if passport.user?
        @users   = new UsersCollection
        @masters = new MastersCollection
        @clients = new ClientsCollection
        @visits  = new VisitsCollection

        @users.fetch
          success: =>
            @masters.fetch
              success: => 
                @clients.fetch
                  success: =>
                    @visits.fetch
                      success: => 
                        @_load()

    _load: ()->
      self = @
    
      @router = new Router()
      
      Backbone.history.start
        pushState: true
        root     : @root

      $("a.navigate").live "click", (e) ->
        el = $ @
        href =
          prop: el.prop("href")
          attr: el.attr("href")
        # Get the absolute root.
        root = location.protocol + "//" + location.host + self.root
        # Ensure the root is part of the anchor href, meaning it's relative.
        if href.prop and href.prop.slice(0, root.length) is root
          # Stop the default event to ensure the link will not cause a page
          # refresh.
          e.preventDefault()
          # `Backbone.history.navigate` is sufficient for all Routers and will
          # trigger the correct events. The Router's internal `navigate` method
          # calls this anyways.  The fragment is sliced from the root.
          Backbone.history.navigate href.attr, true
