depends = [
  'jquery', 
  'underscore', 
  'backbone',
  'text!templates/sms_page.html',
]

define depends, ($, _, Backbone, smsPageTemplate) ->

  class SMSPageView extends Backbone.View

    el: "#content"

    template      : _.template smsPageTemplate
    
    initialize: ->
      @render()

    render: ->
      $(@el).html @template
        clients : app.clients.toJSON()
      return @
