depends = [
  'jquery', 
  'underscore', 
  'backbone',
  'text!templates/masters_page.html',
  'dataTables',
  'twitterBootstrap',
  'parsleyjs'
]

define depends, ($, _, Backbone, mastersPageTemplate) ->

  class MastersPageView extends Backbone.View

    el: "#content"

    template      : _.template mastersPageTemplate

    inputs : [ 
      { text: 'Имя',         id: 'firstName',  dataType: '',        errorMessage: 'Поле не должно быть пустым'}, 
      { text: 'Отчество',    id: 'secondName', dataType: '',        errorMessage: 'Поле не должно быть пустым'}, 
      { text: 'Фамилия',     id: 'lastName',   dataType: '',        errorMessage: 'Поле не должно быть пустым'}, 
      { text: 'E-mail',      id: 'email',      dataType: 'email',   errorMessage: 'Email должен быть правильного формата'}, 
      { text: 'Телефон',     id: 'phone',      dataType: 'phone',   errorMessage: 'Телефон должен быть правильного формата'}, 
      { text: 'Комментарий', id: 'comment',    dataType: '',        errorMessage: 'Поле не должно быть пустым'}, 
      { text: 'Дата начала', id: 'startDate',  dataType: 'dateIso', errorMessage: 'Формат даты: ГГГГ-ММ-ДД'}, 
    ],

    events:
      'click #btn-add-item'     : 'btnCreateMaster'
      'click #btn-modal-save'   : 'saveMaster'
      'click #btn-modal-cancel' : 'closeModalAdd'

    initialize: ->
      @render()

    render: ->
      self = @
      $(@el).html @template
        user: app.user
        masters: app.masters.toJSON()
        inputs: self.inputs
      @initializeTable()
      return @
    
    initializeTable: ->
      self = @
      oDataTable = $('.data-table').dataTable
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "sDom": '<""l>t<"F"fp>',
        fnRowCallback: (nRow, aData, iDisplayIndex, iDisplayIndexFull) ->
          $(nRow).click (e) ->
            if $(e.target).hasClass("item-edit")
              model = app.masters.get(nRow.id)
              self.prepopulateForm(model.toJSON())
              $('#addModal').attr('data-method', 'update')
              self.openModalAdd()
              return
            else if $(e.target).hasClass("item-delete")
              if confirm('Хотите удалить мастера безвозвратно?')
                model = app.masters.get(nRow.id)
                app.masters.remove model
                model.destroy
                  success: () ->
                    self.render()
                  error: () ->
                    self.render()
                return
              
    btnCreateMaster: ->
      $('#addModal').attr('data-method', 'create')
      @openModalAdd()

    openModalAdd: ->
      $('#addModal').modal()
      $('#addModal form').parsley()

    saveMaster: ->
      self = @
      if $('#addModal form').parsley('validate')
        $('#img-loading').removeAttr('hidden')
        method = $('#addModal').attr('data-method')
        if method is 'create'
          app.masters.create
            firstName  : $('#firstName').val()
            secondName : $('#secondName').val()
            lastName   : $('#lastName').val()
            email      : $('#email').val()
            phone      : $('#phone').val()
            comment    : $('#comment').val()
            startDate  : $('#startDate').val()
          , 
          success: (model) =>
            $('#img-loading').attr('hidden', 'hidden')
            model.save()
            self.closeModalAdd()
            self.render()
          error: () ->
            console.log('error', arguments)
            $('#img-loading').attr('hidden', 'hidden')
        else if method is 'update'
          model = app.masters.get $('#id-model').val()
          model.save
            firstName  : $('#firstName').val()
            secondName : $('#secondName').val()
            lastName   : $('#lastName').val()
            email      : $('#email').val()
            phone      : $('#phone').val()
            comment    : $('#comment').val()
            startDate  : $('#startDate').val()
          ,
            success: () ->
              $('#img-loading').attr('hidden', 'hidden')
              self.closeModalAdd()
              self.render()
            error: () ->
              $('#img-loading').attr('hidden', 'hidden')
              console.log 'errrrr', arguments

    prepopulateForm: (model)->
      $('#id-model').val(model['_id'])
      $('#firstName').val(model.firstName)
      $('#secondName').val(model.secondName)
      $('#lastName').val(model.lastName)
      $('#email').val(model.email)
      $('#phone').val(model.phone)
      $('#comment').val(model.comment)
      date = new Date(model.startDate)
      $('#startDate').val(date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate())

    clearForm: ()->
      $('#addModal input').val ''

    closeModalAdd: ()->
      @.clearForm()
      $('#addModal').modal('hide')

