depends = [
  'jquery', 
  'underscore', 
  'backbone',
  'text!templates/clients_page.html',
  'dataTables',
  'twitterBootstrap',
  'parsleyjs'
]

define depends, ($, _, Backbone, clientsPageTemplate) ->

  class ClientsPageView extends Backbone.View

    el: "#content"

    template      : _.template clientsPageTemplate

    inputs : [ 
      { text: 'Имя',         id: 'firstName',  dataType: '',        errorMessage: 'Поле не должно быть пустым'}, 
      { text: 'Отчество',    id: 'secondName', dataType: '',        errorMessage: 'Поле не должно быть пустым'}, 
      { text: 'Фамилия',     id: 'lastName',   dataType: '',        errorMessage: 'Поле не должно быть пустым'}, 
      { text: 'E-mail',      id: 'email',      dataType: 'email',   errorMessage: 'Email должен быть правильного формата'}, 
      { text: 'Телефон',     id: 'phone',      dataType: 'phone',   errorMessage: 'Телефон должен быть правильного формата'}, 
      { text: 'Комментарий', id: 'comment',    dataType: '',        errorMessage: 'Поле не должно быть пустым'}, 
    ],

    events:
      'click #btn-add-item'     : 'btnCreateClient'
      'click #btn-modal-save'   : 'saveClient'
      'click #btn-modal-cancel' : 'closeModalAdd'

    initialize: ->
      @render()

    render: ->
      self = @
      $(@el).html @template
        user: app.user
        clients: app.clients.toJSON()
        inputs: self.inputs
      @initializeTable()
    
    initializeTable: ->
      self = @
      oDataTable = $('.data-table').dataTable
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "sDom": '<""l>t<"F"fp>',
        fnRowCallback: (nRow, aData, iDisplayIndex, iDisplayIndexFull) ->
          $(nRow).click (e) ->
            if $(e.target).hasClass("item-edit")
              model = app.clients.get(nRow.id)
              self.prepopulateForm(model.toJSON())
              $('#addModal').attr('data-method', 'update')
              self.openModalAdd()
              return
            else if $(e.target).hasClass("item-delete")
              if confirm('Хотите удалить мастера безвозвратно?')
                model = app.clients.get(nRow.id)
                app.clients.remove model
                model.destroy
                  success: () ->
                    self.render()
                  error: () ->
                    self.render()
                return
      return @

    btnCreateClient: ->
      $('#addModal').attr('data-method', 'create')
      @openModalAdd()

    openModalAdd: ->
      $('#addModal').modal()
      $('#addModal form').parsley()

    saveClient: ->
      self = @
      if $('#addModal form').parsley('validate')
        $('#img-loading').removeAttr('hidden')
        method = $('#addModal').attr('data-method')
        if method is 'create'
          app.clients.create
            firstName  : $('#firstName').val()
            secondName : $('#secondName').val()
            lastName   : $('#lastName').val()
            email      : $('#email').val()
            phone      : $('#phone').val()
            comment    : $('#comment').val()
          , 
          success: (model) =>
            $('#img-loading').attr('hidden', 'hidden')
            model.save()
            self.closeModalAdd()
            self.render()
          error: () ->
            console.log('error', arguments)
            $('#img-loading').attr('hidden', 'hidden')
        else if method is 'update'
          model = app.clients.get $('#id-model').val()
          model.save
            firstName  : $('#firstName').val()
            secondName : $('#secondName').val()
            lastName   : $('#lastName').val()
            email      : $('#email').val()
            phone      : $('#phone').val()
            comment    : $('#comment').val()
          ,
            success: () ->
              $('#img-loading').attr('hidden', 'hidden')
              self.closeModalAdd()
              self.render()
            error: () ->
              $('#img-loading').attr('hidden', 'hidden')
              console.log 'errrrr', arguments

    prepopulateForm: (model)->
      $('#id-model').val(model['_id'])
      $('#firstName').val(model.firstName)
      $('#secondName').val(model.secondName)
      $('#lastName').val(model.lastName)
      $('#email').val(model.email)
      $('#phone').val(model.phone)
      $('#comment').val(model.comment)

    clearForm: ()->
      $('#addModal input').val ''

    closeModalAdd: ()->
      @.clearForm()
      $('#addModal').modal('hide')

