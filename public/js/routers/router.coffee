depends = [
  'jquery'
  'underscore'
  'backbone'
  'cs!views/main_page'
  'cs!views/masters_page'
  'cs!views/clients_page'
  'cs!views/sms_page'
#  'cs!views/journal_page'
#  'cs!views/statistics_page'
]

#define depends, ($, _, Backbone, MainPageView, MastersPageView, ClientsPageView, JournalPageView, StatisticsPageView) ->
define depends, ($, _, Backbone, MainPageView, MastersPageView, ClientsPageView, SMSPageView) ->

  class Router extends Backbone.Router
  
    initialize: ->
      this.headerClick()
#      new RegisterView()
#      @projectViews = {}

    routes:
      ''                                     : 'index'
      'masters'                              : 'openMasters'
      'clients'                              : 'openClents'
      'journal'                              : 'openJournal'
      'statistics'                           : 'openStatistics'
      'master/:id'                           : 'showMaster'
      'client/:id'                           : 'showClient'
      'sms'                                  : 'showSMS'

#  =============================================================
#  Routes
#  =============================================================

    deletePreviousView: (cb) ->
      console.log('ccc', @currentView)
      @currentView?.undelegateEvents()
      @currentView?.$el.removeData().unbind()
      @currentView?.$el.empty()
      @currentView?.stopListening()
      cb()

    index: ->
      self = @
      @deletePreviousView ()->
        console.log('index')
        self.currentView = new MainPageView()

    openMasters: ->
      @deletePreviousView ()->
        console.log('openMasters')
        self.currentView = new MastersPageView()

    openClents: ->
      @deletePreviousView ()->
        console.log('openClents')
        self.currentView = new ClientsPageView()

    openJournal: ->
      @deletePreviousView ()->
        console.log('openJournal')

    openStatistics: ->
      @deletePreviousView ()->
        console.log('openStatistics')

    showMaster: ->
      @deletePreviousView ()->
        console.log('showMaster')

    showClient: ->
      @deletePreviousView ()->
        console.log('showClient')

    showSMS: ->
      @deletePreviousView ()->
        console.log('showSMS')
        self.currentView = new SMSPageView()

    headerClick: ->
      $('#header-nav > li').click ->
        $('#header-nav > li').removeClass 'active'
        $(this).addClass 'active'
