// Filename: main.js

// Require.js allows us to configure mappings to paths
// as demonstrated below:
require.config({

  paths: {
    jquery: '/js/libs/jquery/jquery-min',
    twitterBootstrap: '/js/libs/twitterBootstrap/bootstrap.min',
    underscore: '/js/libs/underscore/underscore-min',
    backbone: '/js/libs/backbone/backbone',
    ajaxFileUpload: '/js/libs/ajaxFileUpload/ajaxFileUpload',
    dataTables: '/js/libs/dataTables/jquery.dataTables',
    parsleyjs: '/js/libs/parsleyjs/parsley',
    jade: '/js/libs/jade/jade.min',
    text: '/js/libs/require/text',
    cs: '/js/libs/require/cs',
  },
  
  shim: {

    jquery: {
      exports: "$"
    },
  
    underscore: {
      deps: ['jquery'],
      exports: "_"
    },

    backbone: {
      deps: ["underscore", "jquery"],
      exports: "Backbone"
    },
    
    dataTables: {
      deps: ['jquery']
    },

    parsleyjs: {
      deps: ['jquery']
    },

    ajaxFileUpload: {
      deps: ["jquery"]
    },

    twitterBootstrap: {
      deps: ["jquery"]
    },
    
  }

});

require(['cs!views/app'], function(App){
  window.app = new App;
});
